import { Component, OnInit } from '@angular/core';
import { AlertService } from 'src/app/services/alert.service';
import { EnvService } from './../../services/env.service';
import{ HttpServiceService}  from './../../services/http-service.service';
import {Donations_info} from 'src/app/models/donations';

@Component({
  selector: 'app-donations',
  templateUrl: './donations.page.html',
  styleUrls: ['./donations.page.scss'],
})
export class DonationsPage implements OnInit {

  Donations_info:Donations_info;
    constructor(
    private alertService: AlertService,
    private env: EnvService,
    private httpService :HttpServiceService, ){ }
  
    ngOnInit() {
    }
    ionViewWillEnter() {
      this.httpService.makeGet('auth/receive_donation').subscribe(
        Donations_info => {
          this. Donations_info = Donations_info
          if(!this.Donations_info){
            this.Donations_info= {
            furniture :'',
       clothe :''
            }
          }
        },
        error => {
          console.log(error);
        },
        () => {
          
        }
      );
    }
  
    
  
    donationsModal() {
  
      let data = {
        Donations_info: this.Donations_info
      }
      this.httpService.makePost(this.env.API_URL + 'auth/donations', data).subscribe(
        data => {
          this.alertService.presentToast("تم حفظ البيانات بنجاح");
        },
        error => {
          console.log(error);
        },
        () => {
          
        }
      )
  
  
  }}